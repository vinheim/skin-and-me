from typing import Iterable

from flask import request, render_template, flash, redirect, url_for
from flask_debugtoolbar import DebugToolbarExtension

from instance import stage
from models import app, RawIngredient, FinishedProduct, ShippingProvider, \
    FinishedProductType, StoredIngredient, Ingredient, \
    create_full_journey_fixtures
from processes import process__receive_ingredient, \
    process__make_finished_product, process__ship_finished_product, \
    process__store_ingredient, process__store_finished_product
from session import session_manager


def is_product_requirements_met(product_requirements, ingredient_type_totals):
    """
    :param product_requirements: mapping of ingredient type name to required
        amount
    :param ingredient_type_totals: mapping of ingredient type name to current
        amounts
    :return: if requirements met
    """
    for ingredient_type_name, required_amount in product_requirements.items():
        if ingredient_type_totals.get(ingredient_type_name, 0) < required_amount:
            return False

    return True


@app.route('/')
def index():
    return redirect(url_for('goods_in'))


@app.route('/goods_in', methods=['GET', 'POST'])
def goods_in():
    with session_manager() as session:
        if request.method == 'GET':
            raw_ingredients = session.query(RawIngredient)\
                .filter(RawIngredient.amount != 0).all()
            return render_template(
                'goods_in.html', raw_ingredients=raw_ingredients)

        elif request.method == 'POST':
            raw_ingredient = session.query(RawIngredient)\
                .get(request.form['raw_ingredient_id'])
            amount = int(request.form['amount'])
            resp = process__receive_ingredient(session, raw_ingredient, amount)

            if resp is True:
                flash("Success", category='is-success')

                # todo: for trying full journey locally only, can fail
                if app.debug:
                    ingredient = session.query(Ingredient).order_by(Ingredient.id.desc()).first()
                    process__store_ingredient(session, ingredient)
            else:
                flash("Error", category='is-danger')

            return redirect(url_for('goods_in'))


@app.route('/production', methods=['GET', 'POST'])
def production():
    with session_manager() as session:
        if request.method == 'GET':
            # ingredient type name to stored ingredient amount mapping
            ingredient_type_totals = {}

            stored_ingredients: Iterable[StoredIngredient] = session\
                .query(StoredIngredient)\
                .filter(StoredIngredient.is_depleted == False)
            for stored_ingredient in stored_ingredients:
                type_name = stored_ingredient.type.name
                ingredient_type_totals.setdefault(type_name, 0)
                ingredient_type_totals[type_name] += stored_ingredient.get_curr_amount()

            product_types: Iterable[FinishedProductType] = session\
                .query(FinishedProductType).all()

            # mapping of product type name to required ingredients amount,
            # and if requirements met
            product_type_requirements = {}
            for product_type in product_types:
                requirements = product_type.get_requirements_dict()
                product_type_requirements[product_type.name] = {
                    'id': product_type.id,
                    'required_ingredients': requirements,
                    'requirements_met': is_product_requirements_met(
                        requirements, ingredient_type_totals)
                }

            return render_template(
                'production.html',
                ingredient_type_totals=ingredient_type_totals,
                product_type_requirements=product_type_requirements)

        elif request.method == 'POST':
            product_type = session.query(FinishedProductType)\
                .get(request.form['finished_product_type_id'])

            try:
                process__make_finished_product(session, product_type)
                flash("Success", category='is-success')

                # todo: for trying full journey locally only, can fail
                if app.debug:
                    product = session.query(FinishedProduct).order_by(FinishedProduct.id.desc()).first()
                    process__store_finished_product(session, product)
            except:
                flash("Error", category='is-danger')

            return redirect(url_for('production'))


@app.route('/dispatch', methods=['GET', 'POST'])
def dispatch():
    with session_manager() as session:
        if request.method == 'GET':
            stored_products = session.query(FinishedProduct)\
                .filter(FinishedProduct.status == 'Stored').all()
            providers = session.query(ShippingProvider).all()
            return render_template(
                'dispatch.html', stored_products=stored_products,
                providers=providers)

        elif request.method == 'POST':
            product = session.query(FinishedProduct)\
                .get(request.form['stored_product_id'])
            provider = session.query(ShippingProvider)\
                .get(request.form['provider_id'])

            resp = process__ship_finished_product(session, product, provider)

            if resp is True:
                flash("Success", category='is-success')
            else:
                flash("Error", category='is-danger')

            return redirect(url_for('dispatch'))


@app.route('/test__create_fixtures', methods=['GET'])
def create_test_fixtures():
    create_full_journey_fixtures()
    return ''


if __name__ == "__main__":
    if stage == 'dev':
        app.debug = True
        toolbar = DebugToolbarExtension(app)
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    print(app.url_map)
    app.run(debug=True, host='0.0.0.0', port=5000)
