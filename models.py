from typing import Iterable

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from instance import config

app = Flask(__name__)
app.config.from_object(config)
db = SQLAlchemy(app)


class ShippingProvider(db.Model):
    __tablename__ = 'shipping_provider'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)

    products = db.relationship("FinishedProduct", back_populates="shipping_provider")


class IngredientType(db.Model):
    __tablename__ = 'ingredient_type'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    is_mixed = db.Column(db.Boolean, nullable=False)

    stored_ingredients = db.relationship("StoredIngredient", back_populates="type")
    ingredients = db.relationship("Ingredient", back_populates="type")
    raw_ingredients = db.relationship("RawIngredient", back_populates="type")
    finished_product_types = db.relationship("FinishedProductRequirement", back_populates="ing_type")


class WarehouseLocation(db.Model):
    __tablename__ = 'warehouse_location'

    id = db.Column(db.Integer, primary_key=True)
    location = db.Column(db.String(50), unique=True, nullable=False)

    item = db.relationship('StoredIngredient', uselist=False, back_populates="location")
    product = db.relationship('FinishedProduct', uselist=False, back_populates="location")

    @staticmethod
    def get_free_location_or_none(session):
        locs = session.query(WarehouseLocation)\
            .filter(WarehouseLocation.item == None) \
            .filter(WarehouseLocation.product == None)
        if locs.count() == 0:
            return None
        return locs.first()


class StoredIngredient(db.Model):
    __tablename__ = 'stored_ingredient'

    id = db.Column(db.Integer, primary_key=True)
    is_depleted = db.Column(db.Boolean, nullable=False, default=False)

    location_id = db.Column(db.Integer, db.ForeignKey('warehouse_location.id'), nullable=False)
    location = db.relationship('WarehouseLocation', back_populates="item")

    type_id = db.Column(db.Integer, db.ForeignKey('ingredient_type.id'), nullable=False)
    type = db.relationship('IngredientType', back_populates="stored_ingredients")

    ingredients = db.relationship("Ingredient", back_populates="stored_ingredient")
    finished_product_ingredients = db.relationship('FinishedProductIngredient', back_populates="ingredient")

    def get_in_total(self):
        # total amount of ingredients stored in this location after being
        # received
        return sum([ingredient.amount for ingredient in self.ingredients])

    def get_out_total(self):
        # total amount of ingredients taken from this location to make
        # finished products
        return sum([
            ingredient.amount
            for ingredient in self.finished_product_ingredients])

    def get_curr_amount(self):
        return self.get_in_total() - self.get_out_total()


class RawIngredient(db.Model):
    __tablename__ = 'raw_ingredient'

    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer, nullable=False)

    type_id = db.Column(db.Integer, db.ForeignKey('ingredient_type.id'), nullable=False)
    type = db.relationship('IngredientType', back_populates="raw_ingredients")

    def __repr__(self):
        return f"Raw Ingredient: {self.type.name} ({self.amount})"


class Ingredient(db.Model):
    __tablename__ = 'ingredient'

    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer, nullable=False)
    status = db.Column(db.Enum("Accepted", "Stored"), nullable=False, default="Accepted")

    type_id = db.Column(db.Integer, db.ForeignKey('ingredient_type.id'), nullable=False)
    type = db.relationship('IngredientType', back_populates="ingredients")

    stored_ingredient_id = db.Column(db.Integer, db.ForeignKey('stored_ingredient.id'), nullable=True)
    stored_ingredient = db.relationship('StoredIngredient', back_populates="ingredients")


class FinishedProductType(db.Model):
    __tablename__ = 'finished_product_type'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)

    ingredient_types: Iterable['FinishedProductRequirement'] = db.relationship("FinishedProductRequirement", back_populates="prod_type")
    products = db.relationship("FinishedProduct", back_populates="type")

    def get_requirements_dict(self):
        # ingredient type name to required amount mapping
        return {
            assoc.ing_type.name: assoc.amount
            for assoc in self.ingredient_types
        }


class FinishedProduct(db.Model):
    __tablename__ = 'finished_product'

    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.Enum("Created", "Stored", "Shipped"), nullable=False, default="Created")

    type_id = db.Column(db.Integer, db.ForeignKey('finished_product_type.id'), nullable=False)
    type = db.relationship('FinishedProductType', back_populates="products")

    shipping_provider_id = db.Column(db.Integer, db.ForeignKey('shipping_provider.id'), nullable=True)
    shipping_provider = db.relationship('ShippingProvider', back_populates="products")

    location_id = db.Column(db.Integer, db.ForeignKey('warehouse_location.id'), nullable=True)
    location = db.relationship('WarehouseLocation', back_populates="product")

    ingredients = db.relationship('FinishedProductIngredient', back_populates="product")


class FinishedProductIngredient(db.Model):
    __tablename__ = 'finished_product_ingredient'

    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer, nullable=False)

    product_id = db.Column(db.Integer, db.ForeignKey('finished_product.id'), nullable=False)
    product = db.relationship('FinishedProduct', back_populates="ingredients")

    ingredient_id = db.Column(db.Integer, db.ForeignKey('stored_ingredient.id'), nullable=False)
    ingredient = db.relationship('StoredIngredient', back_populates="finished_product_ingredients")


class FinishedProductRequirement(db.Model):
    __tablename__ = 'finished_product_requirements'

    finished_product_type_id = db.Column(db.Integer, db.ForeignKey('finished_product_type.id'), primary_key=True)
    prod_type = db.relationship("FinishedProductType", back_populates="ingredient_types")

    ingredient_type_id = db.Column(db.Integer, db.ForeignKey('ingredient_type.id'), primary_key=True)
    ing_type = db.relationship("IngredientType", back_populates="finished_product_types")

    amount = db.Column(db.Integer, nullable=False)
    

def create_full_journey_fixtures():
    db.drop_all()
    db.create_all()

    garlic_type = IngredientType(name='Garlic', is_mixed=False)
    egg_type = IngredientType(name='Egg', is_mixed=False)
    prod_type = FinishedProductType(name='Fried Egg')

    garlic_req = FinishedProductRequirement(amount=3, ing_type=garlic_type)
    egg_req = FinishedProductRequirement(amount=2, ing_type=egg_type)

    prod_type.ingredient_types.append(garlic_req)
    prod_type.ingredient_types.append(egg_req)

    db.session.add(garlic_type)
    db.session.add(egg_type)
    db.session.add(prod_type)

    db.session.add(garlic_req)
    db.session.add(egg_req)

    db.session.add(RawIngredient(type=garlic_type, amount=200))
    db.session.add(RawIngredient(type=egg_type, amount=200))
    for i in range(20):
        db.session.add(WarehouseLocation(location=f'a{i}'))
    db.session.add(ShippingProvider(name='Shipper'))

    db.session.commit()


if __name__ == "__main__":
    create_full_journey_fixtures()
