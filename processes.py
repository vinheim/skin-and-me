from models import RawIngredient, Ingredient, WarehouseLocation, \
    StoredIngredient, FinishedProductType, IngredientType, \
    FinishedProductIngredient, FinishedProduct, ShippingProvider


def process__receive_ingredient(
        session, raw_ingredient: RawIngredient, amount: int):
    """
    Returns if the amount of the raw ingredient to accept can be accepted
    Ingredient is received with the amount requeste
    """
    if raw_ingredient.amount >= amount:
        raw_ingredient.amount -= amount
        session.add(Ingredient(
            amount=amount,
            type=raw_ingredient.type,
        ))
        session.add(raw_ingredient)
        session.commit()
        return True
    return False


def create_new_stored_ingredient(session, ingredient: Ingredient):
    """
    Returns False if no free location found
    Else uses a free location, creates a new StoredIngredient, and links
      the ingredient to it
    """
    loc = WarehouseLocation.get_free_location_or_none(session)
    if loc is None:
        return False

    existing_ingredient = StoredIngredient(
        location=loc,
        type=ingredient.type,
    )

    ingredient.stored_ingredient = existing_ingredient

    session.add(existing_ingredient)
    session.add(ingredient)
    return True


def store_ingredient_mixed(session, ingredient: Ingredient):
    # Try to find an existing stored ingredient to top up
    existing_ingredients = session.query(StoredIngredient)\
        .filter(StoredIngredient.type == ingredient.type)\
        .filter(StoredIngredient.is_depleted == False)
    existing_ingredient = existing_ingredients.one_or_none()

    # Else create a new one
    if existing_ingredient is None:
        return create_new_stored_ingredient(session, ingredient)

    # Link the ingredient to the stored ingredient
    ingredient.stored_ingredient = existing_ingredient
    session.add(ingredient)
    return True


def store_ingredient_unmixed(session, ingredient: Ingredient):
    # Simply create and link ingredient to new location
    return create_new_stored_ingredient(session, ingredient)


def process__store_ingredient(session, ingredient: Ingredient):
    # Can only store once
    if ingredient.status == 'Stored':
        return False

    if ingredient.type.is_mixed:
        resp = store_ingredient_mixed(session, ingredient)
    else:
        resp = store_ingredient_unmixed(session, ingredient)

    if resp is False:
        return False

    ingredient.status = 'Stored'
    session.add(ingredient)
    session.commit()

    return True


def get_stored_ingredient_with_amount_or_none(
        session, ingredient_type: IngredientType):
    """
    For a single stored ingredient, the available amount is how many
    Ingredients went into it, minus the amount used by
    FinishedProductIngredients
    """
    stored_ingredients = session.query(StoredIngredient)\
        .filter(StoredIngredient.type == ingredient_type) \
        .filter(StoredIngredient.is_depleted == False)

    if stored_ingredients.count() == 0:
        return None, None

    # Get totals
    stored_ingredient: StoredIngredient = stored_ingredients.first()
    total_in_amount = stored_ingredient.get_in_total()
    total_out_amount = stored_ingredient.get_out_total()

    # Sanity check for depleted stored ingredients
    if total_in_amount - total_out_amount == 0:
        stored_ingredient.is_depleted = True
        session.add(stored_ingredient)
        return None, None

    return stored_ingredient, total_in_amount - total_out_amount


def process__make_finished_product(
        session, finished_product_type: FinishedProductType):
    """
    For every product requirement, try to create a number of
    FinishedProductIngredients that represent the requirement filled from 1 or
    more sources of StoredIngredients
    """
    product = FinishedProduct(type=finished_product_type)

    requirements = finished_product_type.ingredient_types
    for requirement in requirements:
        required_amount = requirement.amount

        # Loop trying to get stored ingredients from 1 or more locations to
        # total the required amount asked of by the requirement
        while required_amount > 0:
            stored_ingredient, amount = \
                get_stored_ingredient_with_amount_or_none(
                    session, requirement.ing_type)

            # Undo product creation and finished product ingredient additions
            if stored_ingredient is None:
                raise Exception("Cannot make finished product")

            # If the stored ingredient has more than the required amount,
            # only subtract the required amount, otherwise it is considered
            # depleted
            if amount > required_amount:
                amount = required_amount
            else:
                stored_ingredient.is_depleted = True
                session.add(stored_ingredient)
            required_amount -= amount

            finished_ingredient = FinishedProductIngredient(
                amount=amount, product=product, ingredient=stored_ingredient)
            session.add(finished_ingredient)
    session.commit()


def process__store_finished_product(session, product: FinishedProduct):
    """
    Simply store the finished product if a location is free, and product had
    not been stored
    """
    if product.status != 'Created':
        return False

    loc = WarehouseLocation.get_free_location_or_none(session)
    if loc is None:
        return False

    product.status = 'Stored'
    product.location = loc
    session.add(product)
    session.commit()
    return True


def process__ship_finished_product(
        session, product: FinishedProduct, provider: ShippingProvider):
    """
    Simply ship the finished product if the product is stored
    """
    if product.status != 'Stored':
        return False

    product.shipping_provider = provider
    product.status = 'Shipped'
    session.add(product)
    session.commit()
    return True
