import os
os.environ['STAGE'] = 'test'

from pytest import fixture

from models import db, IngredientType, WarehouseLocation, \
    FinishedProductType, FinishedProductRequirement, FinishedProduct, \
    ShippingProvider
from session import session_manager


@fixture
def session():
    db.drop_all()
    db.create_all()

    with session_manager() as _session:
        yield _session


@fixture
def ingredient_types(session):
    unmixed = IngredientType(
        name='Unmixed Ingredient Type 1',
        is_mixed=False,
    )
    mixed = IngredientType(
        name='Mixed Ingredient Type',
        is_mixed=True,
    )
    session.add(unmixed)
    session.add(mixed)
    session.commit()

    return {'unmixed': unmixed, 'mixed': mixed}


@fixture
def warehouse_locations(session):
    a1 = WarehouseLocation(location='a1')
    a2 = WarehouseLocation(location='a2')

    session.add(a1)
    session.add(a2)
    session.commit()
    
    return {'a1': a1, 'a2': a2}


@fixture
def finished_product_types(session, ingredient_types):
    product_type1 = FinishedProductType(name='Finished Product Type 1')

    req1 = FinishedProductRequirement(amount=1, ing_type=ingredient_types['unmixed'])
    req2 = FinishedProductRequirement(amount=50, ing_type=ingredient_types['mixed'])

    product_type1.ingredient_types.append(req1)
    product_type1.ingredient_types.append(req2)

    session.add(product_type1)
    session.add(req1)
    session.add(req2)
    session.commit()

    return {'product_type1': product_type1}


@fixture
def finished_products(session, finished_product_types):
    product = FinishedProduct(type=finished_product_types['product_type1'])

    session.add(product)
    session.commit()

    return {'product': product}


@fixture
def shipping_providers(session):
    provider = ShippingProvider(name='Shipping Provider')

    session.add(provider)
    session.commit()

    return {'provider': provider}
