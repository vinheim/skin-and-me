from models import Ingredient, WarehouseLocation, FinishedProduct
from processes import process__store_ingredient, \
    process__store_finished_product


def test_free_warehouse_location(
        session, finished_products, ingredient_types, warehouse_locations):

    # Starting with 2 warehouse locations, 1 should be free after the below
    product: FinishedProduct = finished_products['product']
    process__store_finished_product(session, product)

    free_loc = WarehouseLocation.get_free_location_or_none(session)
    assert free_loc is not None

    # After storing the ingredient, there should be no more free spaces
    ingredient1 = Ingredient(
        amount=12,
        type=ingredient_types['unmixed'],
    )
    session.add(ingredient1)
    process__store_ingredient(session, ingredient1)
    assert ingredient1.stored_ingredient.location == free_loc

    assert WarehouseLocation.get_free_location_or_none(session) is None

    # After freeing up the product's location, it should now be free
    prod_loc = product.location
    product.location = None
    session.add(product)

    assert WarehouseLocation.get_free_location_or_none(session) == prod_loc


def test_product_requirements_dict(finished_product_types):
    assert finished_product_types['product_type1'].get_requirements_dict() == {
        'Unmixed Ingredient Type 1': 1,
        'Mixed Ingredient Type': 50,
    }
