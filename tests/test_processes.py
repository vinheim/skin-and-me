from pytest import raises

from models import Ingredient, RawIngredient, FinishedProductType
from processes import process__receive_ingredient, process__store_ingredient, \
    process__make_finished_product, process__store_finished_product, \
    process__ship_finished_product


def test_receive_ingredient(session, ingredient_types):
    raw_ingredient = RawIngredient(
        amount=200,
        type=ingredient_types['unmixed']
    )
    session.add(raw_ingredient)

    ingredients = session.query(Ingredient)
    assert ingredients.count() == 0

    # happy path - amount can be received
    resp = process__receive_ingredient(session, raw_ingredient, 12)
    assert resp is True
    assert raw_ingredient.amount == 188
    assert ingredients.count() == 1
    ingredient = ingredients.one()
    assert ingredient.amount == 12
    assert ingredient.status == 'Accepted'

    # unhappy path - amount > available amount
    resp = process__receive_ingredient(session, raw_ingredient, 200)
    assert resp is False
    assert raw_ingredient.amount == 188
    assert ingredients.count() == 1
    ingredient = ingredients.one()
    assert ingredient.amount == 12
    assert ingredient.status == 'Accepted'


def test_store_unmixed_ingredients(
        session, ingredient_types, warehouse_locations):
    ingredient1 = Ingredient(
        amount=12,
        type=ingredient_types['unmixed'],
    )
    ingredient2 = Ingredient(
        amount=14,
        type=ingredient_types['unmixed'],
    )
    ingredient3 = Ingredient(
        amount=16,
        type=ingredient_types['unmixed'],
    )
    session.add(ingredient1)

    # Store 2 ingredients. As they are unmixed, they should take 2 different
    # locations
    resp = process__store_ingredient(session, ingredient1)
    assert resp is True
    assert ingredient1.stored_ingredient is not None
    loc1 = ingredient1.stored_ingredient.location

    resp = process__store_ingredient(session, ingredient2)
    assert resp is True
    assert ingredient2.stored_ingredient is not None
    loc2 = ingredient2.stored_ingredient.location

    assert loc1 != loc2

    # Only 2 warehouse locations, so 3rd should fail
    resp = process__store_ingredient(session, ingredient3)
    assert resp is False
    assert ingredient3.status == 'Accepted'

    # Can't re-store ingredient
    resp = process__store_ingredient(session, ingredient1)
    assert resp is False


def test_store_mixed_ingredients(
        session, ingredient_types, warehouse_locations):
    ingredient1 = Ingredient(
        amount=12,
        type=ingredient_types['mixed'],
    )
    ingredient2 = Ingredient(
        amount=14,
        type=ingredient_types['mixed'],
    )

    # Store 2 ingredients. As they are mixed, they should share a location
    resp = process__store_ingredient(session, ingredient1)
    assert resp is True
    assert ingredient1.stored_ingredient is not None
    loc1 = ingredient1.stored_ingredient.location

    resp = process__store_ingredient(session, ingredient2)
    assert resp is True
    assert ingredient2.stored_ingredient is not None
    loc2 = ingredient2.stored_ingredient.location

    assert loc1 == loc2

    # Can't re-store ingredient
    resp = process__store_ingredient(session, ingredient1)
    assert resp is False


def test_make_finished_product_unique_ingredient_requirements(
        session, finished_product_types, warehouse_locations,
        ingredient_types):
    """
    Product ingredients have only 1 source of StoredIngredient each
    Depletion is also checked for the unmixed ingredient
    """
    product_type: FinishedProductType = finished_product_types['product_type1']

    mixed_ingredient = Ingredient(
        amount=100,
        type=ingredient_types['mixed']
    )
    unmixed_ingredient = Ingredient(
        amount=1,
        type=ingredient_types['unmixed']
    )
    process__store_ingredient(session, mixed_ingredient)
    process__store_ingredient(session, unmixed_ingredient)

    process__make_finished_product(session, product_type)
    assert unmixed_ingredient.stored_ingredient.is_depleted is True
    assert mixed_ingredient.stored_ingredient.is_depleted is False

    with raises(Exception):
        process__make_finished_product(session, product_type)


def test_make_finished_product_non_unique_ingredient_requirements(
        session, finished_product_types, warehouse_locations,
        ingredient_types):
    """
    Product mixed ingredients are smaller than required amount, but can
    satisfy the requirement together
    Depletion checked on this split case, as well as the unmixed ingredient
    """
    product_type: FinishedProductType = finished_product_types['product_type1']

    mixed_ingredient1 = Ingredient(
        amount=25,
        type=ingredient_types['mixed']
    )
    mixed_ingredient2 = Ingredient(
        amount=25,
        type=ingredient_types['mixed']
    )
    unmixed_ingredient = Ingredient(
        amount=1,
        type=ingredient_types['unmixed']
    )
    process__store_ingredient(session, mixed_ingredient1)
    process__store_ingredient(session, mixed_ingredient2)
    process__store_ingredient(session, unmixed_ingredient)

    process__make_finished_product(session, product_type)
    assert unmixed_ingredient.stored_ingredient.is_depleted is True
    assert mixed_ingredient1.stored_ingredient.is_depleted is True
    assert mixed_ingredient1.stored_ingredient.is_depleted is True

    with raises(Exception):
        process__make_finished_product(session, product_type)


def test_store_finished_product(
        session, finished_products, warehouse_locations):
    """
    Simply test the expected changes of storing a product, and that you can't
    store it again
    """
    product = finished_products['product']

    resp = process__store_finished_product(session, product)
    assert resp is True
    assert product.status == 'Stored'
    assert product.location is not None

    resp = process__store_finished_product(session, product)
    assert resp is False


def test_ship_finished_product(session, finished_products, shipping_providers):
    """
    Simply test the expected changes of shipping a product, and that you can't
    ship it again
    """
    product = finished_products['product']
    provider = shipping_providers['provider']

    # status must be Stored
    resp = process__ship_finished_product(session, product, provider)
    assert resp is False

    product.status = 'Stored'
    resp = process__ship_finished_product(session, product, provider)
    assert resp is True
    assert product.status == 'Shipped'
    assert product.shipping_provider == provider

    # Can't re-ship
    resp = process__ship_finished_product(session, product, provider)
    assert resp is False
