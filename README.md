## Missing pieces required to complete an Ingredient journey outside of the GUI
* Creating the RawIngredient
* Storing the Ingredient after /goods_in
* Storing the FinishedProduct after /production

Possibly /goods_in skips the need for RawIngredient, by acting like a location
where you order in goods.

For the 2 missing 'Storing' processes, perhaps a free WarehouseLocation is
first found before /goods_in or /production, that location is 'locked',
then after the main GUI changes, the relevant store process is done, and the
location is unlocked

## Other things I would have done
* Cleaning form input
* More granular view tests (all currently covered by an end-to-end test)
* Custom exceptions to avoid bare exceptions
* In the interest of keeping things as minimal outside of requirements as 
  possible, there are no JS frameworks / libraries; I would've used React or
  vue.js
* Finding areas to use joinedloads, eg getting totals from foreign models
* Proper error messages in the GUI for when functions return False, eg by
  propagating exceptions
* While not in the requirements, finished products are like raw ingredients
  in that they have a type, location and potentially amount, so it would be 
  nice if you could have intermediary mixtures that require raw ingredients and 
  are used by other finished products, and use polymorphism to handle this. 
  This probably applies to things like cooking though, and not skincare 
  products
  

## Trying the app
* `python models.py`
* `python app.py` - then you're ready to go

## Unit Testing
* `pytest`

## Frontend Testing
* `npm install`
* `python app.py` in 1 terminal
* `npx cypress open` in another terminal