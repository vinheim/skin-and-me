from contextlib import contextmanager

from models import db


@contextmanager
def session_manager():
    with db.Session(db.engine) as _session:
        try:
            yield _session
            _session.commit()
        except:
            _session.rollback()
            raise
        finally:
            _session.close()
