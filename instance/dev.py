import secrets

SQLALCHEMY_DATABASE_URI = 'sqlite:///factory.db'
SQLALCHEMY_TRACK_MODIFICATIONS = False
SECRET_KEY = secrets.token_hex(16)
