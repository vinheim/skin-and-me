import os
from importlib import import_module

stage = os.environ.get("STAGE", "dev")
config = import_module(f"instance.{stage}")
config.DEBUG_TB_INTERCEPT_REDIRECTS = False
