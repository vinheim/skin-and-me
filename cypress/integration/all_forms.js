describe('All forms', () => {
  it('Should allow me to go through the entire journey', () => {
    cy.visit('http://0.0.0.0:5000/test__create_fixtures')

    // Accept fried egg ingredients
    cy.visit('http://0.0.0.0:5000/goods_in');
    cy.get('#raw_ingredients').select('Garlic: 200');
    cy.get('#amount').type('5');
    cy.get('button').click();

    cy.get('.notification').should('contain.text', 'Success');

    cy.get('#raw_ingredients').select('Egg: 200');
    cy.get('#amount').type('5');
    cy.get('button').click();

    cy.get('.notification').should('contain.text', 'Success');

    // Make fried egg
    cy.visit('http://0.0.0.0:5000/production');
    cy.get('button').click();

    cy.get('.notification').should('contain.text', 'Success');

    // Dispatch fried egg
    cy.visit('http://0.0.0.0:5000/dispatch');
    cy.get('#stored_products').select('Fried Egg');
    cy.get('#providers').select('Shipper');
    cy.get('button').click();

    cy.get('.notification').should('contain.text', 'Success');
  })
})